# @open-kappa/mylog

This package provides a simple logging library.
It supports also *node-red* and *typescript*.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mylog)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mylog)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/mylog* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node-red module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
