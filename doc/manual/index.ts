/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:basic_howto.md]]
     */
    "2. Basic HOWTO": void;

    /**
     * [[include:howto_write_custom_loggers.md]]
     */
    "3. HOWTO write custom loggers": void;

    /**
     * [[include:contributing.md]]
     */
    "3. Contributing": void;

    /**
     * [[include:copyright.md]]
     */
    "4. Copyright": void;

    /**
     * [[include:changelog.md]]
     */
    "5. Changelog": void;
}
