# BASIC HOWTO

The library is divided in two blocks: frontend and backend.
The frontend is what you should instantiate, and use to log the messages.

For example, if you like a global singleton in your project, you could do:

```typescript
import {
    MyLogger
} from "@open-kappa/mylog";

/** Singleton instance of the Log class */
export const LOG = new MyLogger();
```

In the code you can then use it as follows:

```typescript
import {LOG} from "./Log"

function foo()
{
    ...
    LOG.warn("Here we are! :)");
}
```

Creating the frontend in this way does not instantiate any backend, and
therefore no actual log is performed.
We can instantiate one backend and register for example, after having read
some configuration, or directly where the Logger instance is created, as we
prefer.

A simple example to instantiate the console logger backend is the following:

```typescript
import {
    MyConsoleLogger
} from "@open-kappa/mylog";
import {LOG} from "./Log"

const consoleLogger = new MyConsoleLogger();
LOG.addLogger(consoleLogger);
```

Please refer to the API documentation to check all possible options for
MyConsoleLogger and other available loggers.
