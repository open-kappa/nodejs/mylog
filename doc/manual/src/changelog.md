# CHANGELOG

* 2.0.2 2023-05-02
    * Updated packages
* 2.0.1 2023-04-28
    * Fixed bug on dependencies
    * Updated packages
* 2.0.0 2023-04-27
    * On redirect, console logger does not uses colors (option provided for
      backward compatibility)
    * Introduced small incompatibilities in types, which should not affect the
      vast majority of thrd-party code
    * Supported both mjs and cjs modules.
    * Updated packages
* 1.3.6 2022-09-03
    * Improved packages
* 1.3.5 2022-09-03
    * Improved .npmrc
    * Fixed previous version
* 1.3.4 2022-09-03
    * Huge code cleanup
    * Updated dependencies
* 1.3.3 2021-07-28
    * WARNING: switched to node-red 2.x.x
    * Updated packages
* 1.3.2 2021-06-04
    * Updated packages
    * Not swithced to latest typescrypt for compatibility with typedoc
    * Switched to typedoc-plugin-pages-fork as temporary fix for tutorials
      generation
* 1.3.1 2020-11-13
    * Added missing files
* 1.3.0 2020-11-13
    * Added support interfaces for MyLogger
* 1.2.0 2020-10-09
    * Added MyCommandLogger
    * Fixed MyFileLogger
* 1.1.5 2020-09-13
    * Updated packages
* 1.1.4 2020-09-10
    * Updated packages
* 1.1.3 2020-09-08
    * Updated packages
* 1.1.2 2020-09-07
    * Updated packages
* 1.1.1 2020-09-07
    * Improved package dependencies
* 1.1.0 2020-09-07
    * Switched to typedoc.
    * Updated packages.
    * Added new logging methods.
    * Added a file logger.
* 1.0.0 2020-03-10
    * First release
