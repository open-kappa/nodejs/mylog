# COPYRIGHT

@open-kappa/mylog - A simple libray for testing

Copyright 2020 Francesco Stefanni

<https://gitlab.com/open-kappa/nodejs/mylog>

@open-kappa/mylog is distributed under the MIT license.
Please see the *license* section to get a copy of the full license.

## License

Copyright (c) 2020 Francesco Stefanni

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Contributors and patreons

The following individuals and institutions are among the contributors:

* Francesco Stefanni: main developer and project initiator.

This library has been sponsored by the following patreons:

* [Gizero Energie s.r.l.](www.gizeroenergie.it).
