# HOWTO WRITE CUSTOM LOGGERS

To implement a custom backend, you must extend the abstract class MyBaseLogger.

For example, the MyConsoleLogger backend has been implemented as follows:

```typescript
class MyConsoleLogger
    extends MyBaseLogger
{
    ...
    constructor(config: IMyConsoleConfig = {})
    {
        super(config);
        ... // this logger specific configurations
    }

    ...
}
```

You can notice that it accepts some configuraiton options, which are implemented
as an interface with optional properties (whenever missing, the default value is
assumed):

```typescript
interface IMyConsoleConfig
    extends IMyBaseConfig
{

    /** Color for debug prints. */
    readonly debugColor?: string;
    ...
}
```

The IMyBaseConfig interface wraps the options for the MyBaseLogger class,
whilst IMyConsoleConfig adds the console logger specific options.

Turning back to the MyConsoleLogger implementation, it must implement the
parent class abstract methods, which are the `*Impl` family:

```typescript
    protected abstract debugImpl(msg: string, level: MyLogLevel): void;

    protected abstract errorImpl(msg: string, level: MyLogLevel): void;

    protected abstract infoImpl(msg: string, level: MyLogLevel): void;

    protected abstract logImpl(msg: string, level: MyLogLevel): void;

    protected abstract traceImpl(msg: string, level: MyLogLevel): void;

    protected abstract warnImpl(msg: string, level: MyLogLevel): void;
```

Please note that the `level` parameter is passed just to get a complete set of
information to the method, but it should not be used to understand when to
perform a print: the methods are already called only when the configured level
mandates to perform an actual print.
