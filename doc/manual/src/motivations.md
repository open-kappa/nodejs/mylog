# MOTIVATIONS

This simple and small library aims to simplify logging.
The main usages are colorized console logs and simple node-red logging.
In the future, other possible extensions will be supported (e.g. file logging).
W.r.t. alternatives, other frameworks are more complete, but this library
aims to be a simple and lightweight "placeholder" in case in the future
something more complex is required.

Its features are:

* Written (and therefore supports) Typescript.
* Supports node-red.
* Clear distinction between the kind of message (log, info, warn, error, etc.)
  and the completeness of the logs (LEVEL_1, LEVEL_2, etc.). This is something
  usually missing, considering the kind also an indicator of completness. We
  believe this separation is more accurate.
* Easy to extend with new loggers.
* Supports assertion logs, i.e. logs performed only in case a condition fails.

Please open issues on [gitlab.com](
https://gitlab.com/open-kappa/nodejs/mylog/issues) to propose new features and
pull requests.
