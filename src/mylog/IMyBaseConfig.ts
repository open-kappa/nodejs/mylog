/* eslint-disable no-undef */
import type {
    MyLogLevel
} from "./mylogImpl.js";


/**
 * Interface for JSON configuration of MyBaseLogger.
 */
export interface IMyBaseConfig
{

    /** Log level for debug. */
    readonly debug?: MyLogLevel;

    /** Log level for error. */
    readonly error?: MyLogLevel;

    /** Log level for info. */
    readonly info?: MyLogLevel;

    /** Log level for log. */
    readonly log?: MyLogLevel;

    /** Whether to skip the indentation. Default is false. */
    readonly skipIndentation?: boolean;

    /** Log level for trace. */
    readonly trace?: MyLogLevel;

    /** Log level for warn. */
    readonly warn?: MyLogLevel;
}
