/* eslint-disable no-undef */
import type {
    IMyBaseConfig
} from "./mylogImpl.js";


/**
 * Interface for JSON configuration of MyCommandLogger.
 */
export interface IMyCommandConfig
    extends IMyBaseConfig
{

    /**
     * The list of commands to execute as scripts.
     * Each string is executed by an exec(), as a single command, and
     * no error is reported.
     * Each command must accept three string parameters:
     * - The log kind (log, info, warn, error...)
     * - The log level (LEVEL_1, LEVEL_2, ...)
     * - The actual (escaped) message.
     */
    readonly commands: Array<string>;
}
