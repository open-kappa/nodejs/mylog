import type {
    IMyLogger,
    MyLogLevel
} from "./mylogImpl.js";


export interface IMyConfigurableLogger
    extends IMyLogger
{

    /**
     * Set verbosity level to all loggers for all streams.
     * @param level - The verbosity level. Default is 2.
     */
    setVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setDebugVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setErrorVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setInfoVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setLogVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setTraceVerbosity(level?: MyLogLevel): void;

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    setWarnVerbosity(level?: MyLogLevel): void;
}
