/* eslint-disable no-undef */
import type {
    IMyBaseConfig
} from "./mylogImpl.js";

/**
 * Allowed colors for MyConsoleLogger.
 */
export type Color =
    "black"
    | "blue"
    | "cyan"
    | "gray"
    | "green"
    | "grey"
    | "magenta"
    | "none"
    | "red"
    | "white"
    | "yellow";

/**
 * Interface for JSON color configuration of MyConsoleLogger.
 */
export interface IMyConsoleConfigColors
{
    /** Color for debug prints. */
    readonly debugColor?: Color;

    /** Color for error prints. */
    readonly errorColor?: Color;

    /** Color for info prints. */
    readonly infoColor?: Color;

    /** Color for log prints. */
    readonly logColor?: Color;

    /** Color fo trace prints. */
    readonly traceColor?: Color;

    /** Color for warn prints. */
    readonly warnColor?: Color;
}

/**
 * Interface for JSON configuration of MyConsoleLogger.
 */
export interface IMyConsoleConfig
    extends IMyBaseConfig, IMyConsoleConfigColors
{
    /** Whether to use colors in redirects (i.e. in no-tty outputs) */
    readonly useColorsInRedirect?: boolean;
}
