/* eslint-disable no-undef */
import type {
    IMyBaseConfig
} from "./mylogImpl.js";


/**
 * Interface for JSON configuration of MyFileLogger.
 */
export interface IMyFileConfig
    extends IMyBaseConfig
{

    /** The output file. */
    readonly outputFile: string;

    /** Whether to overwrite the file. Default is true. */
    readonly append?: boolean;
}
