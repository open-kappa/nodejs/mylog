import type {
    MyLogLevel
} from "./mylogImpl.js";

export interface IMyLogger
{
    /**
     * Assert a debug message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertDebug(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Assert a error message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertError(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Assert a info message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertInfo(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Assert a log message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertLog(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Assert a trace message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertTrace(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Assert a warn message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    assertWarn(
        condition: boolean,
        msg: string,
        level?: MyLogLevel
    ): void;

    /**
     * Log a debug message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    debug(msg: string, level?: MyLogLevel): void;

    /**
     * Log a error message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    error(msg: string, level?: MyLogLevel): void;

    /**
     * Log a info message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    info(msg: string, level?: MyLogLevel): void;

    /**
     * Log a log message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    log(msg: string, level?: MyLogLevel): void;

    /**
     * Log a trace message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    trace(msg: string, level?: MyLogLevel): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseDebug(msg: string): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseError(msg: string): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseInfo(msg: string): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseLog(msg: string): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseTrace(msg: string): void;

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    verboseWarn(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseDebug(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseError(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseInfo(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseLog(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseTrace(msg: string): void;

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    verVerboseWarn(msg: string): void;

    /**
     * Log a warn message.
     * @param msg - The messsage to log.
     * @param level - The level. Default is 1.
     */
    warn(msg: string, level?: MyLogLevel): void;
}
