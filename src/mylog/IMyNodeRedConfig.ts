/* eslint-disable no-undef */
import type {
    IMyBaseConfig
} from "./mylogImpl.js";
import type {
    Node
} from "node-red";


/**
 * Interface for JSON configuration of MyNodeRedLogger.
 */
export interface IMyNodeRedConfig
    extends IMyBaseConfig
{
    /** The reference node. */
    readonly node: Node;
}
