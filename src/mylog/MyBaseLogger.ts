import {
    type IMyBaseConfig,
    MyLogLevel
} from "./mylogImpl.js";


/**
 * The base class for all logger backends.
 */
export abstract class MyBaseLogger
{
    /** The level for debug log. */
    private _debugLevel: MyLogLevel;

    /** The level for error log. */
    private _errorLevel: MyLogLevel;

    /** The current indentation level. */
    private _indentation: number;

    /** The level for info log. */
    private _infoLevel: MyLogLevel;

    /** The level for log log. */
    private _logLevel: MyLogLevel;

    /** Whether to skip the indentaiton */
    private readonly _skipIndentation: boolean;

    /** The indentation string */
    private _spaces: string;

    /** The level for trace log. */
    private _traceLevel: MyLogLevel;

    /** The level for warn log. */
    private _warnLevel: MyLogLevel;

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: IMyBaseConfig)
    {
        this._debugLevel = MyBaseLogger._parseLevel(config, "debug");
        this._errorLevel = MyBaseLogger._parseLevel(config, "error");
        this._infoLevel = MyBaseLogger._parseLevel(config, "info");
        this._logLevel = MyBaseLogger._parseLevel(config, "log");
        this._traceLevel = MyBaseLogger._parseLevel(config, "trace");
        this._warnLevel = MyBaseLogger._parseLevel(config, "warn");

        this._indentation = 0;
        this._skipIndentation = config.skipIndentation ?? false;
        this._spaces = MyBaseLogger._parseSpaces(config);
    }

    /**
     * Log a debug message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public debug(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._debugLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.debugImpl(formattedMsg, level);
    }

    /**
     * Log a error message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public error(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._errorLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.errorImpl(formattedMsg, level);
    }

    /**
     * Get the debug level.
     * @returns The level.
     */
    public getDebugLevel(): MyLogLevel
    {
        return this._debugLevel;
    }

    /**
     * Get the error level.
     * @returns The level.
     */
    public getErrorLevel(): MyLogLevel
    {
        return this._errorLevel;
    }

    /**
     * Get the indentation level.
     * @returns The level.
     */
    public getIndentation(): number
    {
        return this._indentation;
    }

    /**
     * Get the info level.
     * @returns The level.
     */
    public getInfoLevel(): MyLogLevel
    {
        return this._infoLevel;
    }

    /**
     * Get the log level.
     * @returns The level.
     */
    public getLogLevel(): MyLogLevel
    {
        return this._logLevel;
    }

    /**
     * Get the soaces string.
     * @returns The spaces string.
     */
    public getSpaces(): string
    {
        return this._spaces;
    }

    /**
     * Get the trace level.
     * @returns The level.
     */
    public getTraceLevel(): MyLogLevel
    {
        return this._traceLevel;
    }

    /**
     * Get the warn level.
     * @returns The level.
     */
    public getWarnLevel(): MyLogLevel
    {
        return this._warnLevel;
    }

    /**
     * Increments the indentation by one.
     */
    public indent(): void
    {
        this._indentation += 1;
    }

    /**
     * Log a info message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public info(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._infoLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.infoImpl(formattedMsg, level);
    }

    /**
     * Log a log message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public log(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._logLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.logImpl(formattedMsg, level);
    }

    /**
     * Set the debug level.
     * @param level - The level.
     */
    public setDebugLevel(level: MyLogLevel): void
    {
        this._debugLevel = level;
    }

    /**
     * Set the error level.
     * @param level - The level.
     */
    public setErrorLevel(level: MyLogLevel): void
    {
        this._errorLevel = level;
    }

    /**
     * Set the indentation level.
     * @param ind - The level.
     */
    public setIndentation(ind: number): void
    {
        if (ind < 0) return;
        this._indentation = ind;
    }

    /**
     * Set the info level.
     * @param level - The level.
     */
    public setInfoLevel(level: MyLogLevel): void
    {
        this._infoLevel = level;
    }

    /**
     * Set the log level.
     * @param level - The level.
     */
    public setLogLevel(level: MyLogLevel): void
    {
        this._logLevel = level;
    }

    /**
     * Set the spaces string.
     * @param spaces - The spaces string.
     */
    public setSpaces(spaces: string): void
    {
        this._spaces = spaces;
    }

    /**
     * Set the trace level.
     * @param level - The level.
     */
    public setTraceLevel(level: MyLogLevel): void
    {
        this._traceLevel = level;
    }

    /**
     * Set the verbosity level, for all streams.
     * @param level - The verosity level.
     */
    public setVerbosity(level: MyLogLevel): void
    {
        const self = this;
        self.setDebugLevel(level);
        self.setErrorLevel(level);
        self.setInfoLevel(level);
        self.setLogLevel(level);
        self.setTraceLevel(level);
        self.setWarnLevel(level);
    }

    /**
     * Set the warn level.
     * @param level - The level.
     */
    public setWarnLevel(level: MyLogLevel): void
    {
        this._warnLevel = level;
    }

    /**
     * Log a trace message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public trace(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._traceLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.traceImpl(formattedMsg, level);
    }

    /**
     * Decrements the indentation by one.
     */
    public unindent(): void
    {
        if (this._indentation > 0)
        {
            this._indentation -= 1;
        }
    }

    /**
     * Log a warn message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public warn(msg: string, level: MyLogLevel): void
    {
        const self = this;
        if (MyBaseLogger._skipLog(self._warnLevel, level)) return;
        const formattedMsg = self._formatMessage(msg);
        self.warnImpl(formattedMsg, level);
    }

    /**
     * Format the message.
     * @param msg - The message.
     * @returns The formatted string.
     */
    protected _formatMessage(msg: string): string
    {
        const self = this;
        if (self._skipIndentation) return msg;
        let spaces = "";
        for (let i = 0; i < self._indentation; ++i)
        {
            spaces += self._spaces;
        }
        return spaces + msg;
    }

    /**
     * The actual log for debug.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected abstract debugImpl(msg: string, level: MyLogLevel): void;

    /**
     * The actual log for error.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected abstract errorImpl(msg: string, level: MyLogLevel): void;

    /**
     * The actual log for info.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected abstract infoImpl(msg: string, level: MyLogLevel): void;

    /**
     * The actual log for log.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected abstract logImpl(msg: string, level: MyLogLevel): void;

    /**
      * The actual log for trace.
      * @param msg - The formatted message.
      * @param level - The message log level.
      */
    protected abstract traceImpl(msg: string, level: MyLogLevel): void;

    /**
     * The actual log for warn.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected abstract warnImpl(msg: string, level: MyLogLevel): void;

    /**
     * Check and gets a log level.
     * @param config - The configuration.
     * @param name - The field to check.
     * @returns The level to set.
     */
    private static _parseLevel(config: IMyBaseConfig, name: string): MyLogLevel
    {
        const defaultRet = MyLogLevel.LEVEL_1;
        const anyConfig = config as {[name: string]: unknown};
        if (typeof anyConfig[name] !== "number") return defaultRet;
        switch (anyConfig[name])
        {
            case MyLogLevel.DISABLED: return MyLogLevel.DISABLED;
            case MyLogLevel.LEVEL_1: return MyLogLevel.LEVEL_1;
            case MyLogLevel.LEVEL_2: return MyLogLevel.LEVEL_2;
            case MyLogLevel.LEVEL_3: return MyLogLevel.LEVEL_3;
            case MyLogLevel.ALWAYS: return MyLogLevel.ALWAYS;
            default: return defaultRet;
        }
    }

    /**
     * Parses the spaces config.
     * @param config - The configuration.
     * @returns The spaces string to use.
     */
    private static _parseSpaces(config: IMyBaseConfig): string
    {
        const defaultRet = "    ";
        const anyConfig = config as {[name: string]: unknown};
        if (typeof anyConfig.spaces !== "string") return defaultRet;
        return anyConfig.spaces;
    }

    /**
     * Check whether to skip this logging.
     * @param thisLevel - The logger level.
     * @param msgLevel - The level of the message.
     * @returns True if no log must be performed.
     */
    private static _skipLog(
        thisLevel: MyLogLevel,
        msgLevel: MyLogLevel
    ): boolean
    {
        if (thisLevel === MyLogLevel.ALWAYS) return false;
        if (thisLevel === MyLogLevel.DISABLED) return true;
        if (msgLevel === MyLogLevel.ALWAYS) return false;
        if (msgLevel === MyLogLevel.DISABLED) return true;
        return thisLevel < msgLevel;
    }
}
