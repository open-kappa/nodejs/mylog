/* eslint-disable no-console */
import {
    exec,
    foreachPromise
} from "@open-kappa/mypromise";
import {
    type IMyCommandConfig,
    logLevelToString,
    MyBaseLogger,
    type MyLogLevel
} from "./mylogImpl.js";


/**
 * Logger which execs a list of commands to perform the log.
 */
export class MyCommandLogger
    extends MyBaseLogger
{
    /**
     * The list of commands.
     */
    private readonly _commands: Array<string>;

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: IMyCommandConfig)
    {
        super(config);
        this._commands = config.commands;
    }

    /**
     * The actual log for debug.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override debugImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("debug", level, msg);
    }

    /**
     * The actual log for error.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override errorImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("error", level, msg);
    }

    /**
     * The actual log for info.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override infoImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("info", level, msg);
    }

    /**
     * The actual log for log.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override logImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("log", level, msg);
    }

    /**
     * The actual log for trace.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override traceImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("trace", level, msg);
    }

    /**
     * The actual log for warn.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected override warnImpl(msg: string, level: MyLogLevel): void
    {
        this._doLog("warn", level, msg);
    }

    /**
     * Perform the actual log.
     * @param kind - The log kind.
     * @param level - The message log level.
     * @param msg - The formatted message.
     */
    private _doLog(
        kind: string,
        level: MyLogLevel,
        msg: string
    ): void
    {
        const self = this;
        const ecapedMsg = self._escapeMessage(msg);
        const lvl = logLevelToString(level);
        const params = ` ${kind} ${lvl} ${ecapedMsg}`;
        async function singleLog(cmd: string): Promise<void>
        {
            const command = [cmd, params];
            async function onError(): Promise<void>
            {
                return Promise.resolve();
            }
            const executor = exec();
            return executor(command).catch(onError);
        }
        const foreachCommand = foreachPromise(singleLog);
        foreachCommand(self._commands)
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            .catch((_err) => {});
    }

    /**
     * Escape a message for the shell.
     * @param msg - The message.
     * @returns The escaped message.
     */
    private _escapeMessage(msg: string): string
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const escaped = msg.split("'")
            .join("'\"'\"'")
            .split("\n")
            .join("\\n");
        const ret = `'${escaped}'`;
        return ret;
    }
}
