/* eslint-disable no-console */
import {
    type IMyConsoleConfig,
    type IMyConsoleConfigColors,
    MyBaseLogger,
    type MyLogLevel
} from "./mylogImpl.js";
import colors from "colors/safe.js";


/**
 * A console logger with colors.
 */
export class MyConsoleLogger
    extends MyBaseLogger
{
    /** The color for debug */
    private readonly _debugColor: (msg: string) => string;

    /** The color for error */
    private readonly _errorColor: (msg: string) => string;

    /** The color for info */
    private readonly _infoColor: (msg: string) => string;

    /** The color for log */
    private readonly _logColor: (msg: string) => string;

    /** The color for trace */
    private readonly _traceColor: (msg: string) => string;

    /** The color for warn */
    private readonly _warnColor: (msg: string) => string;

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: IMyConsoleConfig = {})
    {
        super(config);

        this._debugColor = MyConsoleLogger._parseColor(
            config,
            "debugColor",
            MyConsoleLogger._noColor
        );
        this._errorColor = MyConsoleLogger._parseColor(
            config,
            "errorColor",
            colors.red
        );
        this._infoColor = MyConsoleLogger._parseColor(
            config,
            "infoColor",
            colors.green
        );
        this._logColor = MyConsoleLogger._parseColor(
            config,
            "logColor",
            MyConsoleLogger._noColor
        );
        this._traceColor = MyConsoleLogger._parseColor(
            config,
            "traceColor",
            MyConsoleLogger._noColor
        );
        this._warnColor = MyConsoleLogger._parseColor(
            config,
            "warnColor",
            colors.yellow
        );
    }

    /**
     * The actual log for debug.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected debugImpl(msg: string, _level: MyLogLevel): void
    {
        console.debug(this._debugColor(msg));
    }

    /**
     * The actual log for error.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected errorImpl(msg: string, _level: MyLogLevel): void
    {
        console.error(this._errorColor(msg));
    }

    /**
     * The actual log for info.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected infoImpl(msg: string, _level: MyLogLevel): void
    {
        console.info(this._infoColor(msg));
    }

    /**
     * The actual log for log.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected logImpl(msg: string, _level: MyLogLevel): void
    {
        console.log(this._logColor(msg));
    }

    /**
     * The actual log for trace.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected traceImpl(msg: string, _level: MyLogLevel): void
    {
        console.trace(this._traceColor(msg));
    }

    /**
     * The actual log for warn.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected warnImpl(msg: string, _level: MyLogLevel): void
    {
        console.warn(this._warnColor(msg));
    }

    /**
     * Utility fake callback for no special color to use.
     * @param msg - The message.
     * @returns The message with the color.
     */
    private static _noColor(msg: string): string
    {
        return msg;
    }

    /**
     * Parse a color configuration.
     * @param config - The configuraiton.
     * @param name - The option to check.
     * @param defaultRet - Default color callback.
     */
    private static _parseColor(
        config: IMyConsoleConfig,
        name: keyof IMyConsoleConfigColors,
        defaultRet: (msg: string) => string
    ): (msg: string) => string
    {
        function getBaseColor(): (msg: string) => string
        {
            const color = config[name];
            if (typeof color === "undefined") return defaultRet;
            switch (color)
            {
                case "black": return colors.black;
                case "red": return colors.red;
                case "green": return colors.green;
                case "yellow": return colors.yellow;
                case "blue": return colors.blue;
                case "magenta": return colors.magenta;
                case "cyan": return colors.cyan;
                case "white": return colors.white;
                case "gray": return colors.gray;
                case "grey": return colors.grey;
                case "none": return MyConsoleLogger._noColor;
            }
            throw new Error(`Unknown color: ${String(color)}`);
        }
        function isStdError(): boolean
        {
            switch (name)
            {
                case "debugColor": return false;
                case "errorColor": return true;
                case "infoColor": return false;
                case "logColor": return false;
                case "traceColor": return true;
                case "warnColor": return true;
            }
            throw new Error(`Unknown logger: ${String(name)}`);
        }
        const useColors = config.useColorsInRedirect === true
            || (isStdError() && process.stderr.isTTY)
            || (isStdError() && process.stdout.isTTY);
        if (useColors)
        {
            return getBaseColor();
        }
        return MyConsoleLogger._noColor;
    }
}
