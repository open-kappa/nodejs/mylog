/* eslint-disable no-console */
import {
    type IMyFileConfig,
    MyBaseLogger,
    type MyLogLevel
} from "./mylogImpl.js";
import fs from "node:fs";

/**
 * A simple file logger.
 */
export class MyFileLogger
    extends MyBaseLogger
{
    /** The output stream */
    private readonly _stream: fs.WriteStream | null;

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: IMyFileConfig)
    {
        super(config);
        function skip(): void
        {
            // ntd
        }
        const append = config.append ?? false;
        this._stream = fs.createWriteStream(
            config.outputFile,
            {
                "flags": append ? "a" : "w"
            }
        );
        this._stream.on("error", skip);
    }

    /**
     * The actual log for debug.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected debugImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * The actual log for error.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected errorImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * The actual log for info.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected infoImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * The actual log for log.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected logImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * The actual log for trace.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected traceImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * The actual log for warn.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected warnImpl(msg: string, _level: MyLogLevel): void
    {
        this._actualImpl(msg);
    }

    /**
     * Actual implementation.
     * @param msg - The formatted message.
     */
    private _actualImpl(msg: string): void
    {
        if (this._stream === null) return;
        this._stream.write(`${msg}\n`);
    }
}
