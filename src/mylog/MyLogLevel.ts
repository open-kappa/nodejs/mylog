/* eslint-disable no-shadow */

/**
 * Enum for possible log levels.
 * Higher level implies that the print is performed only with higher settings.
 */
export enum MyLogLevel
// eslint-disable-next-line @typescript-eslint/indent
{
    DISABLED,
    LEVEL_1,
    LEVEL_2,
    LEVEL_3,
    ALWAYS
}

export function logLevelToString(level: MyLogLevel): string
{
    switch (level)
    {
        case MyLogLevel.DISABLED: return "DISABLED";
        case MyLogLevel.LEVEL_1: return "LEVEL_1";
        case MyLogLevel.LEVEL_2: return "LEVEL_2";
        case MyLogLevel.LEVEL_3: return "LEVEL_3";
        case MyLogLevel.ALWAYS: return "ALWAYS";
        default: return "";
    }
}
