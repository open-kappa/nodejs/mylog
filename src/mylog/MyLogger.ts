import {
    type IMyConfigurableLogger,
    type MyBaseLogger,
    MyLogLevel
} from "./mylogImpl.js";


/**
 * The logger class.
 * It can wrap many actual loggers.
 */
export class MyLogger
implements IMyConfigurableLogger
{
    /** The current indentation. */
    private _indentation: number;
    /** The actual loggers. */
    private readonly _loggers: Array<MyBaseLogger>;
    /** The indentation string */
    private _spaces: string;

    /**
     * Constructor.
     */
    public constructor()
    {
        this._indentation = 0;
        this._loggers = [];
        this._spaces = "    ";
    }

    /**
     * Get the current indentation.
     * @returns The indentation.
     */
    public get indentation(): number
    {
        return this._indentation;
    }

    /**
     * Set the current indentation.
     * @param ind - The indentation.
     */
    public set indentation(ind: number)
    {
        const self = this;
        if (ind <= 0) return;
        self._indentation = ind;
        for (const logger of self._loggers.values())
        {
            logger.setIndentation(ind);
        }
    }

    /**
     * Get the current indentation spaces.
     * @returns The spaces
     */
    public get spaces(): string
    {
        return this._spaces;
    }

    /**
     * Set the current indentation spaces.
     * @param sp - The spacing
     */
    public set spaces(sp: string)
    {
        const self = this;
        self._spaces = sp;
        for (const logger of self._loggers.values())
        {
            logger.setSpaces(sp);
        }
    }

    /**
     * Adds an actual logger.
     * @param logger - An actual logger.
     */
    public addLogger(logger: MyBaseLogger): void
    {
        this._loggers.push(logger);
    }

    /**
     * Assert a debug message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertDebug(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.debug(msg, level);
    }

    /**
     * Assert a error message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertError(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.error(msg, level);
    }

    /**
     * Assert a info message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertInfo(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.info(msg, level);
    }

    /**
     * Assert a log message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertLog(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.log(msg, level);
    }

    /**
     * Assert a trace message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertTrace(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.trace(msg, level);
    }

    /**
     * Assert a warn message.
     * @param condition - False to perform the log.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public assertWarn(
        condition: boolean,
        msg: string,
        level = MyLogLevel.LEVEL_1
    ): void
    {
        if (condition) return;
        this.warn(msg, level);
    }

    /**
     * Log a debug message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public debug(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.debug(msg, level);
        }
        self._loggers.forEach(foo);
    }

    /**
     * Log a error message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public error(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.error(msg, level);
        }
        self._loggers.forEach(foo);
    }

    /**
     * Get the number of registered loggers.
     * @returns The count.
     */
    public getLoggersCount(): number
    {
        return this._loggers.length;
    }

    /**
     * Increase the indentation by one unit.
     */
    public indent(): void
    {
        const self = this;
        self._indentation += 1;
        for (const logger of self._loggers.values())
        {
            logger.setIndentation(self._indentation);
        }
    }

    /**
     * Log an info message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public info(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.info(msg, level);
        }
        self._loggers.forEach(foo);
    }

    /**
     * Log a log message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public log(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.log(msg, level);
        }
        self._loggers.forEach(foo);
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setDebugVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setDebugLevel(level);
        }
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setErrorVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setErrorLevel(level);
        }
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setInfoVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setInfoLevel(level);
        }
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setLogVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setLogLevel(level);
        }
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setTraceVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setTraceLevel(level);
        }
    }

    /**
     * Set verbosity level to all loggers for all streams.
     * @param level - The verbosity level. Default is 2.
     */
    public setVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setVerbosity(level);
        }
    }

    /**
     * Set verbosity level to all loggers.
     * @param level - The verbosity level. Default is 2.
     */
    public setWarnVerbosity(level = MyLogLevel.LEVEL_2): void
    {
        const self = this;
        for (const logger of self._loggers.values())
        {
            logger.setWarnLevel(level);
        }
    }

    /**
     * Log a trace message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public trace(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.trace(msg, level);
        }
        self._loggers.forEach(foo);
    }

    /**
     * Decrease the indentation by one unit.
     */
    public unindent(): void
    {
        const self = this;
        if (self._indentation <= 0) return;
        self._indentation -= 1;
        for (const logger of self._loggers.values())
        {
            logger.setIndentation(self._indentation);
        }
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseDebug(msg: string): void
    {
        this.debug(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseError(msg: string): void
    {
        this.error(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseInfo(msg: string): void
    {
        this.info(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseLog(msg: string): void
    {
        this.log(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseTrace(msg: string): void
    {
        this.trace(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 2.
     * @param msg - The messsage to log.
     */
    public verboseWarn(msg: string): void
    {
        this.warn(msg, MyLogLevel.LEVEL_2);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseDebug(msg: string): void
    {
        this.debug(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseError(msg: string): void
    {
        this.error(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseInfo(msg: string): void
    {
        this.info(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseLog(msg: string): void
    {
        this.log(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseTrace(msg: string): void
    {
        this.trace(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a message, with verbose level 3.
     * @param msg - The messsage to log.
     */
    public verVerboseWarn(msg: string): void
    {
        this.warn(msg, MyLogLevel.LEVEL_3);
    }

    /**
     * Log a warn message.
     * @param msg - The messsage to log.
     * @param level - The level.
     */
    public warn(msg: string, level = MyLogLevel.LEVEL_1): void
    {
        const self = this;
        function foo(logger: MyBaseLogger): void
        {
            logger.warn(msg, level);
        }
        self._loggers.forEach(foo);
    }
}
