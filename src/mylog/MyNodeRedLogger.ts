import {
    type IMyNodeRedConfig,
    MyBaseLogger,
    type MyLogLevel
} from "./mylogImpl.js";
import type {
    Node
} from "node-red";


/**
 * The logger for node-red.
 */
export class MyNodeRedLogger
    extends MyBaseLogger
{
    /** The reference node. */
    private readonly _node: Node;

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: IMyNodeRedConfig)
    {
        super(config);
        this._node = MyNodeRedLogger._parseNode(config);
    }

    /**
     * The actual log for debug.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected debugImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.debug(msg);
    }

    /**
     * The actual log for error.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected errorImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.error(msg);
    }

    /**
     * The actual log for info.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected infoImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.log(msg);
    }

    /**
     * The actual log for log.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected logImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.log(msg);
    }

    /**
     * The actual log for trace.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected traceImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.trace(msg);
    }

    /**
     * The actual log for warn.
     * @param msg - The formatted message.
     * @param level - The message log level.
     */
    protected warnImpl(msg: string, _level: MyLogLevel): void
    {
        this._node.warn(msg);
    }

    /**
     * Parse the node configuration.
     * @param config - The configuration.
     */
    private static _parseNode(config: IMyNodeRedConfig): Node
    {
        if (typeof config.node !== "undefined") return config.node;
        throw new Error(
            "Missing node reference for MyNodeRedLogger configuraiton."
        );
    }
}
