import {
    MyConsoleLogger,
    MyLogger
} from "../mylog/index.js";
import {MyTest} from "@open-kappa/mytest";


class _ThisTest
    extends MyTest
{
    public constructor()
    {
        super("basic tests");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "create test",
            self._createTest.bind(self)
        );
    }

    private _createTest(): void
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        const logger = new MyLogger();
        const consoleLogger = new MyConsoleLogger();
        logger.addLogger(consoleLogger);
        logger.log("Ciao!");
    }
}

const _TEST = new _ThisTest();
_TEST.run();
